import argparse
import numpy as np
import tensorflow as tf

def processFasta(fasta:str):
	from fastaUtils.fasta import parse_fasta
	sequences=[seq.seq for seq in parse_fasta(fasta)]
	sequences=np.array([[s for s in sequence] for sequence in sequences])
	states=[np.unique(sequences[:,idx]).tolist() for idx in range(sequences.shape[1])]

	T=[ np.zeros( (len(states[i+1]),len(states[i])) ) for i in range(len(states)-1) ]
	for i in range(len(states)-1):
		mapi={s:idx for idx,s in enumerate(states[i])}
		mapj={s:idx for idx,s in enumerate(states[i+1])}
		for seq in sequences:
			T[i][mapj[seq[i+1]],mapi[seq[i]]]+=1

	P0=np.zeros( (len(states[0])) )
	map0={s:idx for idx,s in enumerate(states[0])}
	for seq in sequences:
		P0[map0[seq[0]]]+=1

	nseqs=sequences.shape[0]
	P0/=nseqs
	T=[t/nseqs for t in T]
	T=list(reversed(T))

	return sequences,states,P0,T # sequences,states,initial_probability,transition_probabilities

@tf.function
def partitionFunction(e0,E):
	T=[tf.math.exp(e) for e in E]
	p0=tf.math.exp(e0)
	TT=T[0]
	for n in range(1,len(T)):
		TT=tf.tensordot(TT,T[n],axes=1)

	return tf.math.reduce_sum(tf.tensordot(TT,p0,axes=1))

@tf.function
def transitionProbabilities(e0,E):
	with tf.GradientTape(persistent=True) as g1:
		g1.watch(e0)
		for e in E:
			g1.watch(e)
		pf = partitionFunction(e0,E)

	pe0=g1.gradient(pf,e0)/pf
	pe=[g1.gradient(pf,e)/pf for e in E]
	del g1
	return pe0,pe

kl = tf.keras.losses.KLDivergence(reduction=tf.keras.losses.Reduction.SUM)

@tf.function
def loss(e0,E,P0,PT):
	pe0,pe=transitionProbabilities(e0,E)
	loss=kl(P0,pe0)
	for idx,p in enumerate(pe):
		loss+=kl(PT[idx],p)
	return loss

def optimize(e0,E,P0,T,lr=0.1,niter=100):
	import sys
	compute_loss=lambda:loss(e0,E,P0,T)

	opt = tf.optimizers.Adam(lr)
	var_list=[e0]+[e for e in E]
	for i in range(niter):
		opt.minimize(compute_loss, var_list=var_list)
		print("#",i,compute_loss().numpy(),file=sys.stderr)

	return e0,E

# def fittedCooccurrences(e0,E,i,j,si,sj):
# 	T=[tf.nn.softmax(e,axis=0).numpy() for e in E]
# 	p0=tf.nn.softmax(e0,axis=0).numpy()
# 	TT=p0
# 	if i>0:
# 		for k in range(1,i+1):
# 			TT=np.dot(T[-k],TT)

# 	for k in range(TT.shape[0]):
# 		if k!=si:
# 			TT[k]=0.
# 		# else:
# 		# 	TT[k]=1.

# 	for k in range(i+1,j+1):
# 		TT=np.dot(T[-k],TT)

# 	for k in range(TT.shape[0]):
# 		if k!=sj:
# 			TT[k]=0.
# 		# else:
# 		# 	TT[k]=1.

# 	for k in range(j+1,len(T)):
# 		TT=np.dot(T[-k],TT)

# 	num = tf.math.reduce_sum(TT)

# 	TT=T[0]
# 	for n in range(1,len(T)):
# 		TT=np.dot(TT,T[n])

# 	pf=tf.math.reduce_sum(np.dot(TT,p0))

# 	return num/pf

# def fittedCooccurrences_v2(e0,E):
# 	with tf.GradientTape(persistent=True) as g1:
# 		g1.watch(e0)
# 		for e in E:
# 			g1.watch(e)
# 		p0,pe=transitionProbabilities(e0,E)

# 	dp0_de0=g1.gradient(p0,e0)
# 	dp0_dE=[g1.gradient(p0,e) for e in E]
# 	dpe_de0=[g1.gradient(p,e0) for p in pe]
# 	dpe_dE=[[g1.gradient(p,e) for e in E] for p in pe]
# 	del g1
# 	return dp0_de0,dp0_dE,dpe_de0,dpe_dE

def generate(e0,E,states,N=100):
	T=[tf.nn.softmax(e,axis=0).numpy() for e in list(reversed(E))]
	p0=tf.nn.softmax(e0,axis=0).numpy()
	for n in range(N):
		choices=list(range(len(states[0])))
		seq=[ np.random.choice(choices,p=p0) ]
		for i,t in enumerate(T):
			choices=list(range(len(states[i+1])))
			seq.append( np.random.choice(choices,p=t[:,seq[-1]]) )
		seq=[states[i][s] for i,s in enumerate(seq)]
		yield "".join(seq)

def score(fasta,e0,E,states):
	from fastaUtils.fasta import parse_fasta
	aa2idx=[{aa:idx for idx,aa in enumerate(state)} for state in states]
	E1=list(reversed(E))
	for seq in parse_fasta(fasta):
		score=e0[ aa2idx[0][seq.seq[0]] ]
		for n in range(len(E1)):
			score+=E1[n][ aa2idx[n+1][seq.seq[n+1]],aa2idx[n][seq.seq[n]]  ]
		yield seq,-score.numpy()

parser = argparse.ArgumentParser(description='Fit a Potts model to an alignment using a transfer matrix approach and use it to generate new sequences.')
parser.add_argument('--fasta', type=str, help='A fasta file containing aligned sequences')
parser.add_argument('--train', action='store_true', default=False, help='Train the model')
parser.add_argument('--load', type=str, default=None, help='Load saved weights')
parser.add_argument('--epochs','-e', type=int, default=100, help='Number of iterations used to minimize the target function')
parser.add_argument('--learning-rate','-lr', type=float, default=0.1, help='Learning rate')
parser.add_argument('--save', type=str, default=None, help='Learning rate')
parser.add_argument('--generate', type=int, default=None, help='Number of sequences to generate')
parser.add_argument('--score', action='store_true', default=False, help='Score each sequence in input fasta. Score is energy')
args = parser.parse_args()

if args.train:
	sequences,states,P0,T = processFasta(args.fasta)

if args.load is not None:
	import pickle
	with open(args.load, 'rb') as f:
		e0,E,states=pickle.load(f)
else:
	# randomly initialize variables
	E=[tf.Variable(np.random.normal(loc=0,scale=1,size=t.shape)) for t in T]
	e0=tf.Variable(np.random.normal(loc=0,scale=1,size=P0.shape))

if args.train:
	e0,E=optimize(e0,E,P0,T,lr=args.learning_rate,niter=args.epochs)

if args.save is not None:
	import pickle
	with open(args.save, 'wb') as f:
		pickle.dump([e0,E,states], f)

if args.generate is not None:
	for idx,s in enumerate(generate(e0,E,states,N=args.generate)):
		print(">{}".format(idx))
		print(s)

if args.score:
	from fastaUtils.fasta import parse_fasta,parse_header
	for seq,score in score(args.fasta,e0,E,states):
		uid=parse_header(seq.header)[1]
		print(uid,score)