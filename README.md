# Potts model timeseries

Fit and score a categorical timeseries where event E at time t+1 only depends on the event at time t via p(E_{t+1}|E_{t})\propto \exp( E_{t+1}-E_{t} ).
The parameters of the model are learned using a transfer-matrix approach.
Timeseries can then be scored using the trained parameters.